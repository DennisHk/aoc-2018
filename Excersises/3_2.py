from Import.ReadLines import *
import re

def solve(file_name):
    claimed_fields = set()
    who_claimed_field = {}
    untouchables = []
    for i in openFile(file_name):
        coordinates = [int(y) for y in re.search(r'[0-9]+,[0-9]+', i).group().split(',')]
        size = [int(y) for y in re.search(r'[0-9]+x[0-9]+', i).group().split('x')]
        number = int(re.search(r'#[0-9]+', i).group().strip('#'))
        untouchables.append(number)
        for x in range(size[0]):
            for y in range(size[1]):
                new_coordinates = (coordinates[0] + x, coordinates[1] + y)
                if new_coordinates in claimed_fields:
                    if who_claimed_field[new_coordinates] in untouchables:
                        untouchables.remove(who_claimed_field[new_coordinates])
                    if number in untouchables:
                        untouchables.remove(number)
                else:
                    claimed_fields.add(new_coordinates)
                    who_claimed_field[new_coordinates] = number
    print(untouchables)

if __name__ == '__main__':
    solve("3_1")
