from Import.ReadLines import *


def solve(file_name):
    total_two_same = 0
    total_three_same = 0
    for x in openFile("2_1"):
        string_result = {}
        for y in x:
            string_result[y] = string_result.get(y, 0) + 1
        if 2 in string_result.values():
            total_two_same += 1
        if 3 in string_result.values():
            total_three_same += 1
    print(total_two_same * total_three_same)


if __name__ == '__main__':
    solve("2_1")
