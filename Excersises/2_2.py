import sys
from Import.ReadLines import *


def solve(file_name):
    file = openFile(file_name)
    for w in range(len(file)):
        for x in range(len(file)):
            result = 0
            for a,b in zip(file[w], file[x]):
                if a != b:
                    result += 1
            if result == 1:
                for a, b in zip(file[w], file[x]):
                    if a == b:
                        sys.stdout.write(a)
                print("")



if __name__ == '__main__':
    solve("2_1")
