from Import.ReadLines import *
from datetime import datetime, timedelta
import re

def solve(file_name):
    processed_data = []
    minutes_asleep = {}
    total_minutes_asleep = {}
    for x in openFile(file_name):
        date = datetime.strptime(re.search(r'\[(.*?)\]', x).group().strip('[]'), "%Y-%m-%d %H:%M")
        if date.hour == 23:
            date = date.replace(hour=00, minute=00)
            date = date + timedelta(days=1)
        currentActivity = 0;
        if "falls asleep" in x:
            currentActivity = -1
        elif "wakes up" in x:
            currentActivity = -2
        elif "begins shift" in x:
            currentActivity = int(re.search(r'#[0-9]+', x).group().strip('#'))
        processed_data.append((date, currentActivity))
        # print(str(date) + " " + str(currentActivity) + " " + str(date.hour))
    processed_data.sort(key=lambda tup: tup[0])
    soldier = 0
    x = 0
    while x < len(processed_data):
        if processed_data[x][1] > 0:
            soldier = processed_data[x][1]
        asleep = False
        next_stop_point = -1
        if x+1 < len(processed_data) and processed_data[x+1][0].date() == processed_data[x][0].date():
            next_stop_point = processed_data[x+1][0].minute
            x += 1
        for i in range(60):
            if i == next_stop_point:
                asleep = not asleep
                # print(str(processed_data[x + 1][0].date()) + " " + str(processed_data[x][0].date()))
                if x + 1 < len(processed_data) and processed_data[x + 1][0].date() == processed_data[x][0].date():
                    next_stop_point = processed_data[x + 1][0].minute
                    x += 1
            soldier_tuple = (i, soldier)
            if soldier_tuple not in minutes_asleep:
                minutes_asleep[soldier_tuple] = 0
            if soldier not in total_minutes_asleep:
                total_minutes_asleep[soldier] = 0

            if asleep:
                minutes_asleep[soldier_tuple] += 1
                total_minutes_asleep[soldier] += 1

            # get the next stop point, if it exists

        x += 1

    sleepy_soldier = None
    for x in minutes_asleep.keys():
        if sleepy_soldier is None:
            sleepy_soldier = x
        if minutes_asleep[x] > minutes_asleep[sleepy_soldier]:
            sleepy_soldier = x

    print("{} {}".format(sleepy_soldier, minutes_asleep[sleepy_soldier]))
    print(sleepy_soldier[1] * sleepy_soldier[0])


if __name__ == '__main__':
    solve("4_1")
