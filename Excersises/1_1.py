from Import.ReadLines import *


def solve(file_name):
    total = 0
    for x in openFile(file_name):
        if x[:1] == '+':
            total += int(x[1:])
        if x[:1] == '-':
            total -= int(x[1:])
    print(total)


if __name__ == '__main__':
    solve("1_1")
