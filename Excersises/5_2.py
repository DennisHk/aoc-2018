from Import.ReadLines import *
from string import ascii_lowercase

def solve(file_name):
    input_string = openFile(file_name)[0]
    best_letter = (len(input_string) + 1, '')
    for x in ascii_lowercase:
        new_string = input_string.replace(x, "")
        new_string = new_string.replace(x.upper(), "")
        print(new_string)
        new_len = solve_string(new_string)
        if new_len < best_letter[0]:
            best_letter = (new_len, x)
    print(best_letter)



def solve_string(input_string):
    keep_running = True
    while keep_running:
        new_input = find_dupes(input_string)
        if new_input is None:
            keep_running = False
        else:
            input_string = new_input
    return len(input_string)


def find_dupes(input_string):
    lastchar = input_string[0]
    for x in input_string:
        flipped_x = x
        if flipped_x.isupper():
            flipped_x = flipped_x.lower()
        else:
            flipped_x = flipped_x.upper()

        if flipped_x == lastchar:
            v =  input_string.replace(lastchar + x, "")
            return v
        lastchar = x
    return None


if __name__ == '__main__':
    solve("5_1")
