from Import.ReadLines import *

def solve(file_name):
    input_string = openFile(file_name)[0]
    keep_running = True
    while keep_running:
        new_input = find_dupes(input_string)
        if new_input is None:
            keep_running = False
        else:
            input_string = new_input
    print(len(input_string))


def find_dupes(input_string):
    lastchar = input_string[0]
    for x in input_string:
        flipped_x = x
        if flipped_x.isupper():
            flipped_x = flipped_x.lower()
        else:
            flipped_x = flipped_x.upper()

        if flipped_x == lastchar:
            v =  input_string.replace(lastchar + x, "")
            print(lastchar + x)
            return v
        lastchar = x
    return None


if __name__ == '__main__':
    solve("5_1")
