from Import.ReadLines import *
import re


def get_manhattan_distance(start, end):
    x = start[0] - end[0]
    y = start[1] - end[1]
    if x < 0:
        x *= -1
    if y < 0:
        y *= -1
    return x + y


def solve(file_name):
    points = {}
    claimed_fields = {}
    counter = 0;
    for x in openFile(file_name):
        coordinates = []
        for y in x.split(", "):
            coordinates.append(int(y))
        coordinates_tuple = (coordinates[0], coordinates[1])
        points[counter] = coordinates_tuple
        claimed_fields[coordinates_tuple] = 0
        counter += 1
    counter -= 1

    for x in range(1000):
        for y in range(1000):
            closest_area = None
            closest_distance = 10000000
            valid = True
            for z in range(counter):
                distance = get_manhattan_distance((x, y), points[z])
                if distance == closest_distance:
                    valid == False
                elif distance < closest_distance:
                    valid == True
                    closest_area = points[2]
                    closest_distance = distance
            if valid:
                claimed_fields[closest_area] += 1
    print(points)
    print(claimed_fields)



if __name__ == '__main__':
    solve("6_1")
