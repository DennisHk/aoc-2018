from Import.ReadLines import *


def solve(file_name):
    done = False
    pastNumbers = []
    total = 0
    while not done:
        for x in openFile(file_name):
            if x[:1] == '+':
                total += int(x[1:])
            if x[:1] == '-':
                total -= int(x[1:])
            if total in pastNumbers:
                print(total)
                done = True
            pastNumbers.append(total)
            # print(total)


if __name__ == '__main__':
    solve("1_1")
