from Import.ReadLines import *
import re

def solve(file_name):
    claimed_fields = set()
    double_fields = set()
    for i in openFile(file_name):
        coordinates = [int(y) for y in re.search(r'[0-9]+,[0-9]+', i).group().split(',')]
        size = [int(y) for y in re.search(r'[0-9]+x[0-9]+', i).group().split('x')]
        for x in range(size[0]):
            for y in range(size[1]):
                new_coordinates = (coordinates[0] + x, coordinates[1] + y)
                if new_coordinates in claimed_fields:
                    if new_coordinates not in double_fields:
                        double_fields.add(new_coordinates)
                else:
                    claimed_fields.add(new_coordinates)
    print(len(double_fields))


if __name__ == '__main__':
    solve("3_1")
