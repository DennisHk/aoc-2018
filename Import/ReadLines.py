def openFile(fname):
    with open("../Data/" + fname + ".txt") as f:
        content = f.readlines()
    return [x.strip('\n ') for x in content]